'use strict';

// (function mainThemeScript($, Drupal) {
//   Drupal.behaviors.plStarter = {
//     attach(context) {
//       $('html', context).addClass('js');
//     },
//   };
// }(jQuery, Drupal));


(function asideAccordion($, Drupal) {
  Drupal.behaviors.asideAccordion = {
    attach: function attach(context) {
      // @todo: Add better targerting.
      var $accordion = $('.paragraph--type--accordion', context);
      var header = '.field--name-field-accordion-section-title';

      if ($accordion.length < 1) {
        return;
      }
      $accordion.accordion({
        header: header,
        icons: { header: 'ui-icon-plus', activeHeader: 'ui-icon-minus' },
        heightStyle: 'content',
        active: false,
        collapsible: true
      });
    }
  };
})(jQuery, Drupal);
// (function secondaryThemeScript($, Drupal) {
//   Drupal.behaviors.demo2 = {
//     attach(context) {
//       $('html', context).addClass('js2');
//     },
//   };
// }(jQuery, Drupal));
"use strict";
//# sourceMappingURL=script.js.map
