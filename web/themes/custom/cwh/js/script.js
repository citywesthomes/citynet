// (function mainThemeScript($, Drupal) {
//   Drupal.behaviors.plStarter = {
//     attach(context) {
//       $('html', context).addClass('js');
//     },
//   };
// }(jQuery, Drupal));


(function asideAccordion($, Drupal) {
  Drupal.behaviors.asideAccordion = {
    attach(context) {
      // @todo: Add better targerting.
      const $accordion = $('.paragraph--type--accordion', context);
      const header = '.field--name-field-accordion-section-title';

      if ($accordion.length < 1) {
        return;
      }
      $accordion.accordion({
        header,
        icons: { header: 'ui-icon-plus', activeHeader: 'ui-icon-minus' },
        heightStyle: 'content',
        active: false,
        collapsible: true,
      });
    },
  };
}(jQuery, Drupal));
