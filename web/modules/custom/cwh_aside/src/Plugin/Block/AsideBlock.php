<?php
/**
 * @file
 * Contains: AsideBlock
 */

namespace Drupal\cwh_aside\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Aside' Block.
 *
 * @Block(
 *   id = "cwh_aside_block",
 *   category = @Translation("CWH"),
 *   admin_label = @Translation("CWH - Aside block"),
 * )
 */
class AsideBlock extends BlockBase implements BlockPluginInterface {

  /**
   * @var array
   */
  public $fields = ['field_aside'];

  /**
   * {@inherit}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $from = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['cwh_aside_field_target'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Target field'),
      '#description' => $this->t('A comma seperated list of machine names.'),
      '#default_value' => isset($config['cwh_aside_field_target']) ? $config['cwh_aside_field_target'] : '',
    ];
    $form['cwh_aside_parent_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Parent field'),
      '#description' => $this->t('To include items from a parent node, enter the machine name of an entity reference field defining the parent.'),
      '#default_value' => isset($config['cwh_aside_parent_field']) ? $config['cwh_aside_parent_field'] : '',
    ];

    return $form;
  }

  /**
   * {@inherit}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['cwh_aside_field_target'] = $form_state->getValue('cwh_aside_field_target');
    $this->configuration['cwh_aside_parent_field'] = $form_state->getValue('cwh_aside_parent_field');
  }

  /**
   * {@inherit}
   */
  public function getCacheTags() {
    //With this when your node change your block will rebuild
    if ($node = \Drupal::routeMatch()->getParameter('node')) {

      //if there is node add its cachetag
      return Cache::mergeTags(parent::getCacheTags(), ['node:' . $node->id()]);
    }
    else {

      //Return default tags instead.
      return parent::getCacheTags();
    }
  }

  /**
   * {@inherit}
   */
  public function getCacheContexts() {
    //if you depends on \Drupal::routeMatch()
    //you must set context of this block with 'route' context tag.
    //Every new route this block will rebuild
    return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
  }

  /**
   * {@inherit}
   */
  public function build() {
    $config = $this->getConfiguration();
    // Get current node.
    $node = $this->currentNode();
    $build = [];

    if (!empty($config['cwh_aside_field_target'])) {
      $target_fields = $this->configParse($config['cwh_aside_field_target']);
      foreach ($target_fields as $field) {

        $field_value = $this->asideFieldValue($node, $field);
        if ($field_value) {
          $build[] = $field_value;
        }
      }

      // Add class if any items in the build array.
      if ($build && in_array('field_related_document', $target_fields)) {
        $build['#attributes']['class'][] = 'related-documents';
      }
    }

    if (!empty($config['cwh_aside_parent_field'])) {
      $parent_fields = $this->configParse($config['cwh_aside_parent_field']);
      foreach ($parent_fields as $parent_field) {
        $parent = $node->$parent_field->entity;
        $parent_values = $this->asideFieldValue($parent, $field);
        if ($parent_values) {
          $build[] = $parent_values;
        }
      }
    }


    return $build;
  }

  /**
   * Get the current entity, if on a node page.
   *
   * @return mixed|null
   *   Node entity or NULL.
   */
  public function currentNode() {
    if ($node = \Drupal::routeMatch()->getParameter('node')) {

      return $node;
    }
  }

  /**
   * Parse config to usable array.
   *
   * @param $config
   *   Config item to parse.
   *
   * @return array
   *   An array of config items.
   */
  public function configParse($config) {
    return explode(',', $config);
  }

  /**
   * Get the rendered entity for and entity reference field on this node.
   *
   * @param \Drupal\Core\Entity\EntityInterface|NULL $node
   *   A node entity. Uses the current node if not provided.
   * @param string $field
   *   Name of field to get.
   * @param string $view_mode
   *   View mode to render.
   *
   * @return array
   *   A render array for the field or an empty array.
   */
  public function asideFieldValue(EntityInterface $node = NULL, $field = 'field_aside', $view_mode = 'full') {
    $rendered_entities = [];
    // Get current node if not explicitly defined.
    $node = $node ?: $this->currentNode();

    // Check if field is on node.
    if (!$node->hasField($field)) {

      return $rendered_entities;
    }
    else {
      $field_entities = $node->get($field);
      // If our field has data.
      if (!$field_entities->count()) {

        return $rendered_entities;
      }
      else {
        // Loop through all field values.
        foreach ($field_entities as $entity) {
          // @todo: entity_view is deprecated, update to use view builder.
          $rendered_entities[] = entity_view($entity->entity, 'default');
        }

        return $rendered_entities;
      }
    }
  }
}