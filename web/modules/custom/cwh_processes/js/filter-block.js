(function filterBlock($, Drupal) {
  Drupal.behaviors.filterBlock = {

    // Filter results based on query.
    filter: function (selector, query) {
      query = $.trim(query); // Trim white space
      query = query.replace(/ /gi, '|'); // Add OR for regex query

      $(selector).each(function () {
        ($(this).text().search(new RegExp(query, "i")) < 0) ? $(this).hide().removeClass('visible') : $(this).show().addClass('visible');
      });
    },

    attach: function (context) {

      // Define filter input and target.
      const $block = $('.filter-text', context);
      const $grid = $('[data-filter-text]', context);

      if ($block.length < 1 || $grid.length < 1) {

        return;
      }
      //default each row to visible
      $($grid).toggleClass('visible');

      $block.keyup(function (event) {
        //if esc is pressed or nothing is entered
        if (event.keyCode == 27 || $(this).val() == '') {
          //if esc is pressed we want to clear the value of search box
          $(this).val('');

          $($grid).toggleClass('visible').show();

        }
        else {
          Drupal.behaviors.filterBlock.filter($grid, $(this).val());
        }
      });
    },
  };
}(jQuery, Drupal));
