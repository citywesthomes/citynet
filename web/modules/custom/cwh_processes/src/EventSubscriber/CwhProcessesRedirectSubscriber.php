<?php

/**
 * @file
 * Contains \Drupal\cwh_processes\EventSubscriber\CwhProcessesRedirectSubscriber
 */

namespace Drupal\cwh_processes\EventSubscriber;

use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class CwhProcessesRedirectSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return([
      KernelEvents::REQUEST => [
        ['cwhProcessToFirstStep'],
      ]
    ]);
  }

  public function cwhProcessToFirstStep(GetResponseEvent $event) {
    $request = $event->getRequest();


    // This is necessary because this also gets called on
    // node sub-tabs such as "edit", "revisions", etc.  This
    // prevents those pages from redirected.
    if ($request->attributes->get('_route') !== 'entity.node.canonical') {
      return;
    }

    // Only redirect a certain content type.
    if ($request->attributes->get('node')->getType() !== 'process') {
      return;
    }

    // Entity originally navigated to.
    $process_node = $request->attributes->get('node');
    // Any 'field_steps' values.
    $steps = $process_node->get('field_steps')->getValue();

    // Only redirect if process has "steps".
    if (!$steps) {
      return;
    }
    // Get the first item in the array.
    // @todo: Find a cleaner way to do this.
    $step = (reset(reset($steps)));

    $redirect_url = Url::fromUri('entity:node/' . $step);
    $response = new RedirectResponse($redirect_url->toString(), 301);
    $event->setResponse($response);
  }
}
