<?php
/**
 * @file
 * Contains: ProcessStepsBlock
 */

namespace Drupal\cwh_processes\Plugin\Block;

use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Url;

/**
 * Provides a 'Process steps' block.
 *
 * @Block(
 *   id = "cwh_process_steps",
 *   category = @Translation("CWH"),
 *   admin_label = @Translation("CWH: Process - Steps")
 * )
 */
class ProcessStepsBlock extends BlockBase {

  /**
   * {@inherit}
   */
  public function getCacheTags() {
    // With this when your node changes your block will rebuild.
    if ($node = \Drupal::routeMatch()->getParameter('node')) {

      // If this is node add its cachetag
      return Cache::mergeTags(parent::getCacheTags(), ['node:' . $node->id()]);
    }
    else {

      // Return default tags instead.
      return parent::getCacheTags();
    }
  }

  /**
   * {@inherit}
   */
  public function getCacheContexts() {
    // If you depend on \Drupal::routeMatch() you must set context of this block
    // with 'route' context tag.
    // Every new route this block will rebuild.
    return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
  }

  public function build() {
    // Get current node.
    $node = $this->currentNode();
    $steps = [];
    $url_options = ['fragment' => 'main-content'];


    // Get parent process entity.
    $process = $node->field_process->entity;
    $count = 1;

    foreach ($process->field_steps as $step) {
      if ($step->entity) {
        $entity = $step->entity;
        $classes_array = ['step'];

        // Get URL from route.
        $url = Url::fromRoute('entity.node.canonical', ['node' => $entity->id()], $url_options);
        // Get step description.
        $tagline = $entity->field_tagline->value;

        //Ensure there is always a tagline.
        if (!$tagline) {
            $tagline = $entity->label();
        }

        // Set active trail.
        if ($entity->id() == $node->id()) {
          $classes_array[] = 'is-active';
        }

        // Build link.
        $steps[] = [
          '#type' => 'link',
            '#title' => SafeMarkup::format(
                '<span class="step__count">@count</span> <span class="step__tagline">@tagline</span>',
                array(
                    '@count' => $count,
                    '@tagline' => $tagline
                )
            ),
          '#url' => $url,
          '#attributes' => [
            'class' => $classes_array,
          ],
        ];
      }

      $count++;
    }

    return [
      [
        '#theme' => 'cwh_steps_item_list',
        '#list_type' => 'ul',
        '#title' => $process->label(),
        '#items' => $steps,
        '#attributes' => ['class' => ['steps__list']],
      ],
      '#attributes' => ['class' => ['process-steps', 'steps']],
    ];
  }

  /**
   * Get the current entity, if on a node page.
   *
   * @return mixed|null
   *   Node entity or NULL.
   */
  public function currentNode() {
    if ($node = \Drupal::routeMatch()->getParameter('node')) {

      return $node;
    }
  }

}