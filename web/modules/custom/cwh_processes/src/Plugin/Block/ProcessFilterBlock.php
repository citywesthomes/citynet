<?php
/**
 * Created by PhpStorm.
 * User: ashleyhazle
 * Date: 13/10/2017
 * Time: 12:52
 */

namespace Drupal\cwh_processes\Plugin\Block;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Process filter' block.
 *
 * @Block(
 *   id = "cwh_process_filter",
 *   category = @Translation("CWH"),
 *   admin_label = @Translation("CWH: Process - Filter")
 * )
 */
class ProcessFilterBlock extends BlockBase {

  public function build() {
    return [
      [
        '#type' => 'textfield',
        '#title' => $this->t('Filter processes'),
        '#attributes' => [
          'id' => 'cwh-filter-block',
          'class' => ['filter-text', 'form__mega'],
          'placeholder' => 'Search',
        ],
        '#label_attributes' => [
          'for' => 'cwh-filter-block'
        ],
        '#title_display' => 'invisible',
      ],
      '#attached' => [
        'library' => [
          'cwh_processes/filter-block',
        ],
      ],
    ];
  }

}
